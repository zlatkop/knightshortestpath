# README #

OPIS

* web servis za traženje najkraćeg puta između dvije točke za figuru skakača

PREDUVJETI

* java 8 (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* skinuti lokalno [repozitorij](https://bitbucket.org/zlatkop/knightshortestpath/downloads/) te se u komandnoj liniji pozicionirati u target direktorij

UPUTA ZA POKRETANJE IZ KOMANDNOG PROZORA

* u target direktoriju izvršiti: java -jar shortest.path-0.0.1-SNAPSHOT.jar
* u internet preglednik upisati: http://localhost:8080/a1-h8

Implementacija algoritma za traženje najkraćeg puta za skakača preuzeta od:http://yanbraslavsky.blogspot.hr/2016/01/chess-knight-shortest-path-problem-java.html