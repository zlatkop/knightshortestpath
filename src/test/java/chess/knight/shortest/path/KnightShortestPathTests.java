/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chess.knight.shortest.path;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class KnightShortestPathTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void a1_h8KnightLongestPath() throws Exception {
    
        this.mockMvc.perform(get("/a1-h8")).andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.broj_poteza").value(5))
        .andExpect(jsonPath("$.pocetna_pozicija").value("a1"))
        .andExpect(jsonPath("$.potezi[0]").value("c2"))
        .andExpect(jsonPath("$.potezi[1]").value("e3"))
        .andExpect(jsonPath("$.potezi[2]").value("g4"))
        .andExpect(jsonPath("$.potezi[3]").value("h6"))
        .andExpect(jsonPath("$.potezi[4]").value("f7"))
        .andExpect(jsonPath("$.zavrsna_pozicija").value("h8"));
    }
    
    @Test
    public void a1_a1SameStartAndEndPOsition() throws Exception {
    
        this.mockMvc.perform(get("/a1-a1")).andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.broj_poteza").value(0))
        .andExpect(jsonPath("$.pocetna_pozicija").value("a1"))
        .andExpect(jsonPath("$.zavrsna_pozicija").value("a1"));
    }
    
    @Test
    public void e4_a1StartPositionMiddleOfBoard() throws Exception {
    
        this.mockMvc.perform(get("/e4-a1")).andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.broj_poteza").value(2))
        .andExpect(jsonPath("$.pocetna_pozicija").value("e4"))
        .andExpect(jsonPath("$.potezi[0]").value("c5"))
        .andExpect(jsonPath("$.potezi[1]").value("b3"))
        .andExpect(jsonPath("$.zavrsna_pozicija").value("a1"));
    }
    
    @Test
    public void g8_h8NeighborPositionTopLeftOnBoard() throws Exception {
    
        this.mockMvc.perform(get("/g8-h8")).andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.broj_poteza").value(2))
        .andExpect(jsonPath("$.pocetna_pozicija").value("g8"))
        .andExpect(jsonPath("$.potezi[0]").value("e7"))
        .andExpect(jsonPath("$.potezi[1]").value("g6"))
        .andExpect(jsonPath("$.zavrsna_pozicija").value("h8"));
    }
    
    @Test
    public void a1_H8WrongFormatKnightLongestPath() throws Exception {
    
        this.mockMvc.perform(get("/a1-H8")).andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.greska").value("Pogrešan format zahtjeva prema servisu!"));
    }

}
