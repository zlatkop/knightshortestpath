package chess.knight.shortest.path.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import chess.knight.shortest.path.domain.KnightShortestPathError;
import chess.knight.shortest.path.manager.KnightShortestPathManager;

@RestController
public class KnightShortestPathController {

    @RequestMapping("/*")
    public String knightPath(HttpServletRequest req) throws JsonProcessingException {
    	String path = req.getRequestURI().substring(1);
    	Pattern pattern = Pattern.compile("[a-h][1-8]-[a-h][1-8]");
		Matcher matcher = pattern.matcher(path);
		ObjectMapper mapper = new ObjectMapper();
		if(!matcher.matches()){
			return mapper.writerWithDefaultPrettyPrinter()
	        		.writeValueAsString(new KnightShortestPathError("Pogrešan format zahtjeva prema servisu!"));
		}
    	int delimitterLocation = path.indexOf("-");
    	System.out.println("delimitterLocation=" + delimitterLocation);
    	String startPoint = path.substring(0, delimitterLocation);
    	String destinationPoint = path.substring(delimitterLocation + 1, path.length());
    	System.out.println(mapper.writerWithDefaultPrettyPrinter()
        		.writeValueAsString(new KnightShortestPathManager().
        				shortestPath(startPoint, destinationPoint)));
    	return mapper.writerWithDefaultPrettyPrinter()
        		.writeValueAsString(new KnightShortestPathManager().
        				shortestPath(startPoint, destinationPoint));
    }
}
