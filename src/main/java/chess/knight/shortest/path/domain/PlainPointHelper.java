package chess.knight.shortest.path.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class PlainPointHelper {

	
	private PlainPointHelper(){
		//prevent instance
	}
	
	public static String vertexXPoint2ChessLetterPoint(int x){
		switch(x){
			case 0: return "a";
			case 1: return "b";
			case 2: return "c";
			case 3: return "d";
			case 4: return "e";
			case 5: return "f";
			case 6: return "g";
			case 7: return "h";
			default: return "UNKOWN";
		}
	}
	
	public static String vertexYPoint2ChessNumberPoint(int y){
		switch(y){
			case 0: return "1";
			case 1: return "2";
			case 2: return "3";
			case 3: return "4";
			case 4: return "5";
			case 5: return "6";
			case 6: return "7";
			case 7: return "8";
			default: return "UNKOWN";
		}
	}
	
	public static boolean isValidChessPoint(final String point){
		Pattern pattern = Pattern.compile("[a-h][1-8]");
		Matcher matcher = pattern.matcher(point);
		return matcher.matches();
	}
	
	
	
	public static int chessLetterPoint2VertexXPoint(final String point){
		switch(point.substring(0, 1)){
			case "a": return 0;
			case "b": return 1;
			case "c": return 2;
			case "d": return 3;
			case "e": return 4;
			case "f": return 5;
			case "g": return 6;
			case "h": return 7;
			default: return -1;
		}
	}
	
	public static int chessNumberPoint2VertexYPoint(final String point){
		switch(point.substring(1)){
			case "1": return 0;
			case "2": return 1;
			case "3": return 2;
			case "4": return 3;
			case "5": return 4;
			case "6": return 5;
			case "7": return 6;
			case "8": return 7;
			default: return -1;
		}
	}
}
