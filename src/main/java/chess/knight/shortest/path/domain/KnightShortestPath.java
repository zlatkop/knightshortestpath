package chess.knight.shortest.path.domain;

import java.util.List;

public class KnightShortestPath {
	
	private int broj_poteza;
	private String pocetna_pozicija;
	private List<String> potezi;
	private String zavrsna_pozicija;
	
	public KnightShortestPath(int broj_poteza, String pocetna_pozicija, List<String> potezi, String zavrsna_pozicija) {
		this.broj_poteza = broj_poteza;
		this.pocetna_pozicija = pocetna_pozicija;
		this.potezi = potezi;
		this.zavrsna_pozicija = zavrsna_pozicija;
	}
	public String getZavrsna_pozicija() {
		return zavrsna_pozicija;
	}
	public List<String> getPotezi() {
		return potezi;
	}
	public String getPocetna_pozicija() {
		return pocetna_pozicija;
	}
	public int getBroj_poteza() {
		return broj_poteza;
	}
	@Override
	public String toString() {
		return "KnightShortestPath [broj_poteza=" + broj_poteza 
				+ ", pocetna_pozicija=" + pocetna_pozicija + ", potezi="
				+ potezi + ", zavrsna_pozicija=" + zavrsna_pozicija + "]";
	}

}
