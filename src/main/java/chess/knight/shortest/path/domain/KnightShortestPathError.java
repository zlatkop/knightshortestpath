package chess.knight.shortest.path.domain;

public class KnightShortestPathError {
	private String greska;

	public KnightShortestPathError(String greska) {
		this.greska = greska;
	}

	public String getGreska() {
		return greska;
	}

	@Override
	public String toString() {
		return "KnightShortestPathError [greska=" + greska + "]";
	}
	
}
